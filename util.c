#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "util.h"

void __attribute__((noreturn)) __attribute__((format(printf,1,2)))
die(char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  fputs("rozvrhovatko: ", stderr);
  vfprintf(stderr, fmt, args);
  fputc('\n', stderr);
  exit(1);
}

void *
xmalloc(int size)
{
  void *p = malloc(size);
  if (!p)
    die("Failed to allocate %d bytes of memory", size);
  memset(p, 0, size);
  return p;
}

void *
xrealloc(void *ptr, int size)
{
  void *p = realloc(ptr, size);
  if (!p)
    die("Failed to re-allocate %d bytes of memory", size);
  return p;
}

struct v
{
	int size;
	int max;
	char data[];
};

void vsetsize(void ** v, int s)
{
	if(s<0) s = 0;
	if(!*v)
	{
		int max = s;
		struct v *this = xmalloc(sizeof(struct v) + max);
		*v = &this->data;
		memset(*v, 0, max);
		this->max = max;
		this->size = s;
	}
	else
	{
		struct v *this = (struct v*)*v - 1;
		if(this->max < s)
		{
			int max = this->max * 2;
			if(max < s) max = s;
			this = xrealloc(this,sizeof(struct v) + max);
			*v = &this->data;
			if(this->max < max) memset(*v + this->max, 0, max - this->max);
			this->max = max;
		}
		this->size = s;
	}
}

void vreserve(void ** v, int max)
{
	if(max<0) max=0;
	if(!*v)
	{
		struct v *this = xmalloc(sizeof(struct v) + max);
		*v = &this->data;
		memset(*v, 0, max);
		this->max = max;
		this->size = 0;
	}
	else
	{
		struct v *this = (struct v*)*v - 1;
		if(max < this->size) max = this->size;
		if(max == this->max) return;
		this = xrealloc(this,sizeof(struct v) + max);
		*v = &this->data;
		if(this->max < max) memset(*v + this->max, 0, max - this->max);
		this->max = max;
	}
}

void vaddsize(void ** v, int s)
{
	vsetsize(v,vsize(*v)+s);
}

int vsize(void * v)
{
	if(!v) return 0;
	struct v *this = (struct v*)v - 1;
	return this->size;
}

int vmax(void * v)
{
	if(!v) return 0;
	struct v *this = (struct v*)v - 1;
	return this->max;
}

void vfree(void ** v)
{
	if(!*v) return;
	struct v *this = (struct v*)*v - 1;
	free(this);
	*v = 0;
}

void * vclone(void * v)
{
	if(!v) return 0;
	struct v *this = (struct v*)v - 1;
	int l = sizeof(struct v) + this->max;
	struct v *new = xmalloc(l);
	memcpy(new,this,l);
	return new->data;
}



int FORMAT_CHECK(printf,2,3) vprint(VECTOR(char) *out , char *fmt, ...)
{
	va_list args;
	int size = VSIZE(*out);
	if(size <= 0)
	{
		VSETSIZE(*out,++size);
	}
	int max = VMAX(*out);
	int n = max-size+1;
	va_start(args, fmt);
	int r = vsnprintf(*out+size-1, max-size+1, fmt, args);
	va_end(args);
	if(r >= n)
	{
		VRESERVE(*out, size+r);
		va_start(args, fmt);
		r = vsprintf(*out+size-1, fmt, args);
		va_end(args);
	}
	if(r>0) VADDSIZE(*out, r);
	return r;
}

char * newstr(char * in)
{
	int l = strlen(in);
	char * r = xmalloc(l+1);
	memcpy(r,in,l);
	r[l]=0;
	return r;
}



#include "csv.h"
#include<stdbool.h>


static char dash = ',';
static char nl = '\n';
static char quotes = '"';
char csv_dash = ',';
char csv_nl = '\n';
char qcsv_uotes = '"';

csv csv_load_from_str(char * in)
{
#define END VPB(VLAST(VLAST(r)),'\0')
	csv r = 0;
	VPB(r,0);
	VPB(r[0],0);

	bool inQuotes=0;
	for(int i=0;in[i];i++)
	{
		if(inQuotes)
		{
			if(in[i]==quotes && in[i+1]==quotes) {VPB(VLAST(VLAST(r)),in[i++]);}
			else
			if(in[i]==quotes) inQuotes = 0;
			else
				VPB(VLAST(VLAST(r)),in[i]);
		}
		else
		{
			if(in[i]==nl)
			{
				if(in[i+1]) {END;VPB(r,0);VPB(VLAST(r),0);}
			}
			else
			if(in[i]==dash) {END;VPB(VLAST(r),0);}
			else
			if(in[i]==quotes) inQuotes=1;
			else
				VPB(VLAST(VLAST(r)),in[i]);
		}
	}
	END;
	return r;
}

csv csv_load_from_FILE(FILE * f)
{
	VECTOR(char) str = 0;
	int c;
	while((c = fgetc(f))!=EOF) VPB(str,c);
	csv r = csv_load_from_str(str);
	VFREE(str);
	return r;
}

void csv_free(csv in)
{
	FORV(i, in) FORV(j, *i) VFREE(*j);
	FORV(i, in) VFREE(*i);
	VFREE(in);
}

void csv_printstr(FILE *f, char * s)
{
	fprintf(f,"%s",s);
}

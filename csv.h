#include "util.h"
#include<stdio.h>

typedef VECTOR(VECTOR(VECTOR(char))) csv;

csv csv_load_from_FILE(FILE * f);
void csv_free(csv);

void csv_printstr(FILE *f, char * s);

extern char csv_dash;
extern char csv_nl;
extern char csv_quote;

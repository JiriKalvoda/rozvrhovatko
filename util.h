#ifndef UTIL_H
#define UTIL_H
#define fo(a,b) for(int a=0;a<(b);++a)
typedef long long ll;


#define NONRET __attribute__((noreturn))
#define FORMAT_CHECK(func,i,j) __attribute__((format(func,i,j)))

void NONRET FORMAT_CHECK(printf,1,2) die(char *fmt, ...);

#ifdef DEBUG
#define DBG(f...) fprintf(stderr,f)
#else
#define DBG(f...) do { } while(0)
#endif

void *xmalloc(int size);
void *xrealloc(void *ptr, int size);

// Vector data contain header (size and allocated size - max) and data array with unspecified size
//
// All vector function take pointer to first element of data part of vector
// (null pointer represent unallocated vector)
// or pointer to pointer to first element if vector can be reallocated
//
// size in number of bites
void vsetsize(void ** v, int s);
void vaddsize(void ** v, int s);
void vreserve(void ** v, int s);
int vsize(void * v);
int vmax(void * v);
void vfree(void ** v);
void * vclone(void * v);

// Macros for multiple size by size of element
#define VSETSIZE(v,s) {vsetsize((void **)&(v),(int)sizeof(*(v))*(s));}
#define VADDSIZE(v,s) {vaddsize((void **)&(v),(int)sizeof(*(v))*(s));}
#define VRESERVE(v,s) {vreserve((void **)&(v),(int)sizeof(*(v))*(s));}
#define VSIZE(v) (vsize(v)/(int)sizeof(*(v)))
#define VMAX(v) (vmax(v)/(int)sizeof(*(v)))
#define VLAST(v) ((v)[VSIZE(v)-1])
#define VPB(v,a) {VADDSIZE(v,1);VLAST(v)=a;}
#define VFREE(v) vfree((void **)&(v))
#define VCLONE(v) ((typeof(v)) vclone((void *)(v)))
#define VECTOR(a) a *

#define FORV(a, v) for(typeof(v) a=v;a < v + VSIZE(v);a++)


int FORMAT_CHECK(printf,2,3) vprint(VECTOR(char) *out , char *fmt, ...);

char * newstr(char * in);


#endif

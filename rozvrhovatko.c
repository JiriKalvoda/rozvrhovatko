#include "csv.h"
#include<stdbool.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include <getopt.h>

#define popcnt __builtin_popcount

typedef unsigned char uch;

#define MAX_SUBJ 64
#define MAX_BLOCK 16
typedef uch subj_ind_t;
typedef uch block_ind_t;
typedef short int member_ind_t;
typedef unsigned short int block_mask_t;
typedef short int relation_ind_t;

block_ind_t * relation_block;

struct Relation
{
	member_ind_t member;
	subj_ind_t subj;
	bool teacher;
	relation_ind_t relation;
};

struct multi_subj
{
	subj_ind_t subj;
	member_ind_t open_in_block[MAX_BLOCK];
	member_ind_t member_in_block[MAX_BLOCK];
} * multi_subj;
subj_ind_t multi_subj_len;

struct subj
{
	char * name;
	bool * collision_matrix;
	int num_group;
	VECTOR(subj_ind_t) collision;
	VECTOR(struct Relation) member;
	block_mask_t allowedBlock;
	block_mask_t mask;
	subj_ind_t multi_subj;
} * subj;
subj_ind_t subj_len;


struct member
{
	block_mask_t allowedBlock;
	char * name;
	VECTOR(struct Relation) subj;
	block_mask_t mask;
} * member;
int member_len;

int max_block = 0;

int read_num(char ** in)
{
	int r=0;
	for(;'0'<=**in && **in<='9';in[0]++) r=r*10+**in-'0';
	return r;
}
block_mask_t parse_mask(char * in)
{
	if(!in || !in[0]) return ~0;
	block_mask_t r=0;
	//printf("%s -> " , in);
	while(in[0])
	{
		int x = read_num(&in);
		x--;
		if(*in=='-')
		{
			in++;
			int y=read_num(&in);
			for(int i=x;i<y;i++) r|=1<<i;
		}
		else r|=1<<x;
		assert(*in=='\0' || *in==';');
		if(*in) in++;
	}
	//printf("%d\n",r);
	return r;
}

void load(char * file_name)
{
	const int header_len=2;
	const int begin_len=2;
	FILE * f = stdin;
	if(file_name) f = fopen(file_name,"r");
	if(!f) die("File %s couldn't be opened.",file_name);
	csv in = csv_load_from_FILE(f);
	if(file_name) fclose(f);
	fo(i,VSIZE(in))
	{
		if(VSIZE(in[i])!=VSIZE(in[0]))
		{
			fprintf(stderr,"Input error: Line %d have %d colums (shud have %d)\n",i,VSIZE(in[i]), VSIZE(in[0]));
			exit(1);
		}
	}
	subj_len = VSIZE(in[0])-begin_len;
	subj = xmalloc(sizeof(subj[0])*subj_len);
	fo(i,subj_len) subj[i].name = newstr(in[0][i+begin_len]);
	fo(i,subj_len) subj[i].collision_matrix = xmalloc(sizeof(bool) * subj_len);
	member_len = VSIZE(in)-header_len;
	member = xmalloc(sizeof(member[0])*member_len);
	fo(i,subj_len) subj[i].allowedBlock=parse_mask(in[1][i+begin_len]);
	int relation_len=0;
	fo(i,member_len)
	{
		member[i].name = newstr(in[i+header_len][0]);
		member[i].allowedBlock = parse_mask(in[i+header_len][1]);
		fo(j,subj_len)
		{
			for(char * it=in[i+header_len][j+begin_len];*it;it++)
			{
				struct Relation rel={.member=i, .subj=j, .teacher=0, .relation=relation_len++};
				if(*it=='v') subj[j].num_group++, rel.teacher=1;
				VPB(member[i].subj,rel);
				VPB(subj[j].member,rel);
			}
		}
	}
	relation_block = malloc(sizeof(block_ind_t)*relation_len);
	csv_free(in);


	multi_subj_len=0;
	fo(i,subj_len) if(subj[i].num_group>1)
		subj[i].multi_subj = multi_subj_len++;
	multi_subj = malloc(sizeof(multi_subj[0]) * multi_subj_len);
	fo(i,subj_len) if(subj[i].num_group>1)
		multi_subj[subj[i].multi_subj].subj=i;
}

void calc_collision()
{
	fo(i,member_len)
	{
		FORV(x, member[i].subj) if(subj[x->subj].num_group==1) subj[x->subj].allowedBlock &= member[i].allowedBlock;
		FORV(x, member[i].subj) 
		FORV(y, member[i].subj) 
			if(x!=y && subj[x->subj].num_group==1 && subj[y->subj].num_group==1)
				subj[x->subj].collision_matrix[y->subj]=1;
	}
	fo(i,subj_len) fo(j,subj_len) if(subj[i].collision_matrix[j])
		VPB(subj[i].collision,j);
}

void print_matrix()
{
	printf("%d\n", subj_len);
	fo(i,subj_len)
	{
		fo(j,subj_len) putchar(subj[i].collision_matrix[j]?'X':'.');
		printf(" %5s[%d:%d] %d\n",subj[i].name,subj[i].num_group,VSIZE(subj[i].member), subj[i].allowedBlock);
	}
}

bool is_done[MAX_SUBJ];
bool is_done_multi[MAX_SUBJ];

ll solutions_len;
ll all_solutions_len;

int jumpUpPropability=0;
int try_find_multisub=10;

bool disable_permutation=1;
bool force_disable_permutation=0;

void print_solution(block_mask_t * mask, int used_blocks)
{
	printf("\n");
	printf("\n");
	fo(i,subj_len) fo(j,subj_len) if(subj[i].collision_matrix[j])
		if(subj[i].num_group==1) if(subj[j].num_group==1)
			if(mask[i]&mask[j])
				printf("COLISION %s[%d] %s[%d]\n",subj[i].name,mask[i], subj[j].name,mask[j]), assert(0);
	//fo(i,subj_len) fo(j,subj_len) if(subj[i].collision_matrix[j])
		//assert(mask[i]!=mask[j]);
	//int used[MAX_SUBJ];
	//fo(i,subj_len) used[i]=0;
	//fo(act_block,used_blocks)
		//fo(i,subj_len) if(mask[i]==1<<act_block)
			//used[i]++;
	//fo(i,subj_len) assert(used[i]==1);

	printf("Solution without multi-subject %lld:\n",solutions_len);
	fo(act_block,used_blocks)
	{
		bool first=1;
		fo(i,subj_len) if(mask[i]==1<<act_block)
		{
			printf(first?"%s":" %s",subj[i].name);
			first=0;
		}
		printf("\n");
	}
}

char * out_file_name = "solution-XXXXX-YYYY.csv";

void print_all_solution()
{
	printf("\n");
	printf("Final solution %lld-%lld:\n",solutions_len,all_solutions_len);
	VECTOR(char) block_str[MAX_BLOCK];
	fo(i,MAX_BLOCK) block_str[i]=0;
	fo(i,subj_len)
	{
		int g=0;
		FORV(it, subj[i].member) if(it->teacher)
		{
			vprint(block_str+relation_block[it->relation],
					subj[i].num_group==1?"%s ":"%s[%d] ",
					subj[i].name, g++);
		}
	}
	fo(i,max_block) printf("%s\n",block_str[i]?:"");

	printf("\n");

	int over_optimal=0;
	fo(i,multi_subj_len)
	{
		int act_subj = multi_subj[i].subj;
		printf("%s: ", subj[act_subj].name);
		int max_in_group = 0;
		fo(j,max_block)
		{
			int  o = multi_subj[i].open_in_block[j];
			int  m = multi_subj[i].member_in_block[j];
			if(m) assert(o);
			if(o)
			{
				int in_group = (m-1)/o+1;
				if(in_group > max_in_group) max_in_group = in_group;
			}
		}
		int opt = (VSIZE(subj[act_subj].member) - 1) / subj[act_subj].num_group  +1 -1;
		printf(" Maximum: %2d Optimal: %2d  ",max_in_group, opt);
		over_optimal += max_in_group-opt;
		fo(j,max_block)
		{
			int  o = multi_subj[i].open_in_block[j];
			int  m = multi_subj[i].member_in_block[j];
			if(o)
				printf("%d:[%d -> %2d] ",j+1 ,o ,m);
		}
		printf("\n");

	}
	printf("Over optimal: %d\n",over_optimal);

	char * fname = malloc(strlen(out_file_name)+1);
	strcpy(fname, out_file_name);
	{
		int j=all_solutions_len;
		for(int i=strlen(fname);i>=0;i--)
			if(fname[i]=='Y') fname[i]=j%10+'0',j/=10;
		if(j)
		{
			fprintf(stderr,"Output file name doesn't contain enought 'Y'\n");
			exit(1);
		}
	}
	{
		int j=solutions_len;
		for(int i=strlen(fname);i>=0;i--)
			if(fname[i]=='X') fname[i]=j%10+'0',j/=10;
		if(j)
		{
			fprintf(stderr,"Output file name doesn't contain enought 'X'\n");
			exit(1);
		}
	}
	FILE * f = fopen(fname,"w");
	if(!f)
	{
		fprintf(stderr,"File %s couldn't be opened\n",fname);
		exit(1);
	}
	free(fname);
	fo(i,subj_len) putc(csv_dash,f), csv_printstr(f,subj[i].name);
	putc(csv_nl,f);
	fo(i,member_len)
	{
		csv_printstr(f,member[i].name);
		int j=0;
		fo(s,subj_len)
		{
			putc(csv_dash,f);
			int k=0;
			while(j<VSIZE(member[i].subj) && member[i].subj[j].subj == s) 
				fprintf(f,k++?";%d":"%d",relation_block[member[i].subj[j].relation]+1),
					j++;
		}
		putc(csv_nl,f);
	}
	fclose(f);
	all_solutions_len++;
}

void balance()
{
	fo(i,multi_subj_len)
	{
		int act_subj = multi_subj[i].subj;
		fo(j,max_block) multi_subj[i].open_in_block[j] = multi_subj[i].member_in_block[j] = 0;
		FORV(it, subj[act_subj].member)
			(it->teacher?multi_subj[i].open_in_block:multi_subj[i].member_in_block)
					[relation_block[it->relation]]++;
	}
	bool change = 1;
	while(change)
	{
		change = 0;
		fo(i,multi_subj_len)
		{
			member_ind_t * member_in_block = multi_subj[i].member_in_block;
			member_ind_t * open_in_block = multi_subj[i].open_in_block;
			int act_subj = multi_subj[i].subj;
			FORV(it, subj[act_subj].member) if(!it->teacher)
			{
				int old_b = relation_block[it->relation];
				fo(b, max_block) if(member[it->member].mask & (1<<b)) if(open_in_block[b])
					if(member_in_block[old_b]*open_in_block[b] > (member_in_block[b]+1)*open_in_block[old_b])
				{
					change = 1;
					member_in_block[old_b]--;
					member_in_block[b]++;
					member[it->member].mask |= (1<<old_b);
					member[it->member].mask &= ~(1<<b);
					relation_block[it->relation] = b;
					break;
				}
			}
		}
	}
	print_all_solution();
}

bool move_to_other(int m, int bl, bool write)
{
	//printf("move(%d,%d,%d)\n",m,bl,write);
	FORV(it, member[m].subj) if(relation_block[it->relation]==bl && subj[it->subj].num_group>1 && is_done_multi[it->subj])
	{
		block_mask_t pos_mask = member[m].mask & subj[it->subj].mask;
		if(pos_mask)
		{
			if(write)
			{
				fo(i,max_block) if((1<<i) & pos_mask)
				{
					relation_block[it->relation]=i;
					member[m].mask &= ~(1<<relation_block[it->relation]);
					return 1;
				}
			}
			return 1;
		}
	}
	return 0;
}


void find_multisubj()
{
	int act_subj=-1;
	{
		int optVal=1<<10;
		int optLen=0;
		fo(i, subj_len) if(!is_done_multi[i]) if(subj[i].num_group<=optVal)
		{
			if(subj[i].num_group<optVal) optVal=subj[i].num_group, optLen=0;
			optLen++;
		}
		if(optLen==0) {balance();return;}
		optLen = rand()%optLen;
		fo(i, subj_len) if(!is_done_multi[i]) if(subj[i].num_group==optVal)
		{
			if(!optLen) act_subj=i;
			optLen--;
		}
	}


	int open_in_block[MAX_BLOCK];
	fo(i,MAX_BLOCK) open_in_block[i]=0;
	int opt_max_in_block = 1<<30;
	int min_max_in_block = (VSIZE(subj[act_subj].member) - subj[act_subj].num_group-1)/subj[act_subj].num_group + 1;
	bool go=0;
	bool end_r()
	{
		int in_block[MAX_BLOCK];
		fo(i,MAX_BLOCK) in_block[i]=0;
		int max_in_block=min_max_in_block;
		//printf("ZDE1\n");
		block_mask_t m=0;
		fo(i,max_block) if(open_in_block[i]) m|= 1<<i;
		/*FORV(rel, subj[act_subj].member)
		{
			if(!rel->teacher && !(m &  member[rel->member].mask))
			{
				if(popcnt(member[rel->member].mask)<=2) printf("%d %d %s\n",m,member[rel->member].mask,member[rel->member].name);
				return 0;
			}
		}*/
	//printf("ZDE2 %d\n",act_subj);
		int num_moved=0;
		FORV(rel, subj[act_subj].member) if(!rel->teacher)
		{
			bool place(struct Relation * rel)
			{
				fo(i,max_block) if(member[rel->member].mask & (1<<i))
					if(in_block[i] < max_in_block*open_in_block[i])
					{
						in_block[i]++;
						relation_block[rel->relation]=i;
						return 1;
					}
				return 0;
			}
			bool place_move(struct Relation * rel)
			{
				fo(i,max_block)
					if(in_block[i] < max_in_block*open_in_block[i])
						if(move_to_other(rel->member, i, 0))
					{
						num_moved=1;
						in_block[i]++;
						relation_block[rel->relation]=i;
						return 1;
					}
				return 0;
			}
			if(!place(rel))
			{
				max_in_block++;
				if(!place(rel))
				{
					max_in_block--;
					if(!place_move(rel))
					{
						max_in_block++;
						if(!place_move(rel)) return 0;
					}
				}
			}
		}
		//printf("%c%d -> max: %d [%d] * %d = %d member: %d\n",go?'#':'$',act_subj, max_in_block,min_max_in_block, subj[act_subj].num_group, max_in_block*subj[act_subj].num_group, VSIZE(subj[act_subj].member) - subj[act_subj].num_group);
		max_in_block += 3 * num_moved;
		if(max_in_block < opt_max_in_block)
			opt_max_in_block = max_in_block;
		if(go && max_in_block == opt_max_in_block)
		{
			FORV(rel, subj[act_subj].member) if(!rel->teacher)
			{
				if(!(member[rel->member].mask & (1<<relation_block[rel->relation])))
					assert(move_to_other(rel->member, relation_block[rel->relation], 1));
				else
					member[rel->member].mask &= ~(1<<relation_block[rel->relation]);
			}
	is_done_multi[act_subj]=1;
			{
				//printf("%s -> ",subj[act_subj].name);fo(i,max_block) fo(j,open_in_block[i]) printf("%d ",i+1);printf("\n");
				subj[act_subj].mask=0;
				fo(i,max_block) if(open_in_block[i]) subj[act_subj].mask |= 1<<i;
				find_multisubj();
			}
	is_done_multi[act_subj]=0;
			FORV(rel, subj[act_subj].member) if(!rel->teacher) member[rel->member].mask |= (1<<relation_block[rel->relation]);
			return 1;
		}
		if(!go && max_in_block <= min_max_in_block) return 1;
		return 0;
	}
	//printf("ZDE0 %d\n",act_subj);
	unsigned int rand_seed;
	bool r(int i)
	{
		while(i<VSIZE(subj[act_subj].member) && !subj[act_subj].member[i].teacher) i++;
		//printf("VS %d %d\n",i,VSIZE(subj[act_subj].member));
		if(i==VSIZE(subj[act_subj].member))
		{
			return end_r();
		}
		struct Relation * rel = subj[act_subj].member+i;
		fo(b,max_block)
		{
			if(subj[act_subj].allowedBlock & (1<<b))
			if(member[rel->member].mask & (1<<b))
			{
				if(subj[act_subj].num_group>=4)
					if(rand_r(&rand_seed)%100<50) continue;
				relation_block[rel->relation]=b;
				member[rel->member].mask &= ~(1<<b);
				open_in_block[b]++;
				bool ret = r(i+1);
				open_in_block[b]--;
				member[rel->member].mask |= (1<<b);
				if(ret) return ret;
			}
		}
		return 0;
	}
	int start_rand_seed=123*rand();
	rand_seed = start_rand_seed;
	r(0);
	if(opt_max_in_block >= 1<<20)
	{
		printf("%s couldn't be placed.\n", subj[act_subj].name);
	}
	else
	{
		go=1;
		rand_seed = start_rand_seed;
		r(0);
	}

}

void find_multisubj_begin(block_mask_t * mask,int used_blocks)
{
	fo(i,member_len) member[i].mask=((1<<max_block)-1) & member[i].allowedBlock;
	fo(i, subj_len)
	{
		if(subj[i].num_group!=1) continue;
		assert(popcnt(mask[i])==1);
		block_ind_t b = popcnt(mask[i]-1);
		FORV(it,subj[i].member)
		{
			relation_block[it->relation]=b;
			member[it->member].mask &= ~(1<<b);
		}
	}
	print_solution(mask, used_blocks);
	all_solutions_len=0;
	fo(i,try_find_multisub)
		find_multisubj();
	solutions_len++;
}


int find(block_mask_t * mask, int used_blocks)
{
	int act_subj=-1;
	{
		int opt=1<<30;
		fo(i,subj_len)
		{
			int score = popcnt(mask[i]);
			score += (used_blocks<max_block) * popcnt(subj[i].allowedBlock) << 10;
			if(!is_done[i] && score < opt)
			{
				opt=score;
				act_subj=i;
			}
		}
	}
	if(act_subj == -1)
	{
		find_multisubj_begin(mask, used_blocks);
		if(!jumpUpPropability)  return 0;
		if(jumpUpPropability>=1000) return 1<<30;
		int jumpUp=0;
		while(random()%1000 < jumpUpPropability) jumpUp++;
		return jumpUp;
	}
	else
	{
		//printf("S %d %d %d\n",act_subj, mask[act_subj],used_blocks);
		is_done[act_subj]=1;
		block_mask_t mask_new[MAX_SUBJ];
		fo(act_block, used_blocks<max_block?used_blocks+1:used_blocks)
			if(mask[act_subj]>>act_block & 1)
			{
				memcpy(mask_new,mask,MAX_SUBJ * sizeof(block_mask_t));
				mask_new[act_subj]=1<<act_block;
				bool is_possible=1;
				FORV(i,subj[act_subj].collision)
				{
					mask_new[*i]&= ~(1<<act_block);
					is_possible &= (bool)mask_new[*i];
					//if(!mask_new[*i]) printf("NO %s\n", subj[*i].name);
				}
				//printf("B %d %d\n",act_block,is_possible);
				if(is_possible)
				{
					int used_blocks_new = used_blocks;
					if(act_block>=used_blocks) used_blocks_new = act_block+1;
					int jumpUp = find(mask_new,used_blocks_new);
					if(jumpUp)
					{
						is_done[act_subj]=0;
						return jumpUp-1;
					}
				}
			}
		is_done[act_subj]=0;
	}
	return 0;
}

void find_begin()
{
	block_mask_t mask[MAX_SUBJ];
	fo(i,subj_len)
	{
		if(subj[i].num_group!=1) is_done[i]=1;
		if(subj[i].num_group<=1) is_done_multi[i]=1;
	}
	fo(i,subj_len) mask[i] = ((1<<max_block)-1) & subj[i].allowedBlock;
	find(mask,disable_permutation?0:max_block);
}


static const char short_opts[] = "haj:b:t:o:pP";
static const struct option long_opts[] = {
  { "help",		no_argument,	NULL,	'h' },
  { "jump",		required_argument,		NULL,	'j' },
  { "block",		required_argument,		NULL,	'b' },
  { "try",		required_argument,		NULL,	't' },
  { "out",		required_argument,		NULL,	'o' },
  { NULL,		0,			NULL,	0   },
};

static void NONRET
usage(char * name)
{
  fprintf(stderr, "Usage: %s <options> [input-file]\n\n\
Options:\n\
\t-h --help \t\tPrint this help\n\
\t-t --try <int>\t\tFor each place of single-sub do <int> try of map multi-subject\n\
\t-b --block <int>\tNumber of block to use\n\
\t-j --jump <int 0-1000>\tProbability of skip some solution after find some solution\n\
\t-o --out <path>\t\tCSV output file name (XXX and YY is mandatory placeholder for id)\n\
\t-p\t\t\tDo not disable permutation\n\
\t-P\t\t\tForce disable permutation (danger!)\n\
",name);
  exit(0);
}

char * in_file_name;

static void
parse_opts(int argc, char **argv)
{
	int opt;
	optind = 0;
	while ((opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) >= 0)
	{
		switch (opt)
		{
			case 'h':
				usage(argv[0]);
				break;
			case 'a':
				jumpUpPropability=0;
				break;
			case 'j':
				jumpUpPropability=atoi(optarg);
				break;
			case 'b':
				max_block=atoi(optarg);
				break;
			case 't':
				try_find_multisub=atoi(optarg);
				break;
			case 'o':
				out_file_name = optarg;
				break;
			case 'p':
				disable_permutation = 0;
				break;
			case 'P':
				force_disable_permutation = 1;
				break;
			default:
				fprintf(stderr,"\n");
				usage(argv[0]);
		}
	}
	if(optind < argc-1) usage(argv[0]);
	if(optind < argc)
		in_file_name = argv[optind];
}

int main(int argc, char ** argv)
{
	parse_opts(argc,argv);
	load(in_file_name);
	calc_collision();
	print_matrix();
	assert(subj_len < MAX_SUBJ);
	if(disable_permutation) fo(i,subj_len) if(subj[i].allowedBlock != (1<<popcnt(subj[i].allowedBlock))-1)
	{
		printf("Disable permutation couldn't be activated [%d]\n\n",subj[i].allowedBlock);
		disable_permutation=0;
		break;
	}
	if(force_disable_permutation) disable_permutation=1;
	if(max_block)
		find_begin();
	else
	for(max_block=1;max_block<=MAX_BLOCK;max_block++)
	{
		printf("MAX_BLOCK: %d\n",max_block);
		find_begin();
		if(solutions_len) break;
	}
	return 0;
}

VERSION=1.1

SHELL := $(shell which bash)

GCCVERSION := $(shell gcc -dumpversion | cut -f1-1 -d.)


MYPATH := $(shell realpath . )

#CFLAGS+=-Wall -Wno-psabi -g3
CFLAGS+=-O3
CFLAGS+=-funroll-loops -march=native 
ifeq ($(shell test $(GCCVERSION) -gt 9; echo $$?),0)
CPPFLAGS+=-Wformat-overflow=0 -Wformat-truncation=0  -Wno-psabi
endif
LDFLAGS := -lm
BIN=/usr/bin

CC=gcc


all: rozvrhovatko
.PHONY: all


BINFILS=rozvrhovatko

UTIL_DEP=util.h
util.o: util.c $(UTIL_DEP)
CSV_DEP=$(UTIL_DEP) csv.h
csv.o: csv.c $(CSV_DEP)

rozvrhovatko.o: rozvrhovatko.c $(CSV_DEP)

UTIL_O=util.o
CSV_O=$(UTIL_O) csv.o
ROZVRHOVATKO_O=$(CSV_O) rozvrhovatko.o

rozvrhovatko: $(ROZVRHOVATKO_O)

%.o:
	$(CC) $(CFLAGS)  $*.c -c -o $@


install: all
	for i in $(BINFILS); do if [[ -e $$i ]]; then cp -f $$i $(BIN) || exit $$? ; fi; done
uninstall:
	for i in $(BINFILS); do if [[ -e $(BIN)/$$i ]]; then rm $(BIN)/$$i || exit $$? ; fi; done

.PHONY: install uninstall


clean:
	rm -f *~ *.o $(BINFILS) 
.PHONY: clean


%.pdf: %.tex
	pdfcsplain $*.tex

